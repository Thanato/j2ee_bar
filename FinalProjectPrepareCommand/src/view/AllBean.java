package view;

import java.util.Collection;
import java.util.Date;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import command.Command;
import command.CommandManagerRemote;
import controler.EjbLocator;

@Named("allBean")
@ApplicationScoped
public class AllBean {
	private CommandManagerRemote commandManager;
	private Collection<command.Command> commands=null;
	
	private Integer idSelected;
	
	private Integer id;
	private Integer tableId;
	private Date takenAt;
	private Date readyAt;
	private Date finishedAt;
	private Date payedAt;
	private Collection<menu.Menu> menus;
	
	public AllBean()
	{
		listingCommands();
	}
	
	public void listingCommands()
	{	
		//System.out.println("Récupération du moduleManager");
		commandManager=EjbLocator.getLocator().getCommandManager();
		if(commandManager!=null)
		{
			commands=commandManager.all();
		}
	}
	
	public void load ()
    {
    	if (idSelected != null) {
    		Command command = commandManager.find(idSelected); 
	        this.setId(command.getId());
	        this.setTableId(command.getTableId());
	        this.setTakenAt(command.getTakenAt());
	        this.setReadyAt(command.getReadyAt());
	        this.setFinishedAt(command.getFinishedAt());
	        this.setPayedAt(command.getPayedAt());
	        this.setMenus(command.getMenus());
    	}
    }
	
	public void refresh()
	{
		if(commandManager!=null)
		{
			id = null;
			tableId = null;
			takenAt = null;
			readyAt = null;
			finishedAt = null;
			payedAt = null;
			menus = null;
			
			commands=commandManager.all();
		}
	}
	
	public Integer getSelectedCommandId() {
		return idSelected;
	}

	public void setSelectedCommandId(Integer selectedCommandId) {
		idSelected = selectedCommandId;
	}
	
	public Collection<command.Command> getCommands() {
		return commands;
	}
	public void setCommands(Collection<command.Command> commands) {
		this.commands = commands;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getTakenAt() {
		return takenAt;
	}

	public void setTakenAt(Date takenAt) {
		this.takenAt = takenAt;
	}

	public Integer getTableId() {
		return tableId;
	}

	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}

	public Date getReadyAt() {
		return readyAt;
	}

	public void setReadyAt(Date readyAt) {
		this.readyAt = readyAt;
	}

	public Date getFinishedAt() {
		return finishedAt;
	}

	public void setFinishedAt(Date finishedAt) {
		this.finishedAt = finishedAt;
	}

	public Date getPayedAt() {
		return payedAt;
	}

	public void setPayedAt(Date payedAt) {
		this.payedAt = payedAt;
	}

	public Collection<menu.Menu> getMenus() {
		return menus;
	}

	public void setMenus(Collection<menu.Menu> menus) {
		this.menus = menus;
	}
}
