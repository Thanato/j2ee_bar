package view;

import java.util.Collection;
import java.util.Date;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import command.Command;
import command.CommandManagerRemote;
import controler.EjbLocator;

@Named("indexBean")
@ApplicationScoped
public class IndexBean {
	private CommandManagerRemote commandManager;
	
	private Collection<command.Command> commands=null;
	
	private Integer idSelected;
	
	private Integer id;
	private Integer tableId;
	private Date takenAt;
	private Collection<menu.Menu> menus;
	
	public IndexBean()
	{
		listingTakenCommands();
	}
	
	public void listingTakenCommands()
	{	
		//System.out.println("Récupération du moduleManager");
		commandManager=EjbLocator.getLocator().getCommandManager();
		if(commandManager!=null)
		{
			commands=commandManager.taken();
		}
	}
	
	public void load ()
    {
    	if (idSelected != null) {
    		Command command = commandManager.find(idSelected); 
	        this.setId(command.getId());
	        this.setTableId(command.getTableId());
	        this.setTakenAt(command.getTakenAt());
	        this.setMenus(command.getMenus());
    	}
    }
	
	public void finish() 
	{
		if (idSelected != null) {
    		Command command = commandManager.find(idSelected); 
    		command.setReadyAt(new Date());
    		commandManager.saveOrUpdate(command);
    		
    		refresh();
    	}
	}
	
	public void refresh()
	{
		if(commandManager!=null)
		{
			id = null;
			tableId = null;
			takenAt = null;
			menus = null;
			
			commands=commandManager.taken();
		}
	}
	
	public Collection<command.Command> getCommands() {
		return commands;
	}
	public void setCommands(Collection<command.Command> commands) {
		this.commands = commands;
	}
	
	public Integer getSelectedCommandId() {
		return idSelected;
	}

	public void setSelectedCommandId(Integer selectedCommandId) {
		idSelected = selectedCommandId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTableId() {
		return tableId;
	}

	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}



	public Collection<menu.Menu> getMenus() {
		return menus;
	}

	public void setMenus(Collection<menu.Menu> menus) {
		this.menus = menus;
	}

	public Date getTakenAt() {
		return takenAt;
	}

	public void setTakenAt(Date takenAt) {
		this.takenAt = takenAt;
	}

}
