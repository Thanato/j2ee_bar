package view;

import java.util.Collection;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import menu.Menu;
import menu.MenuManagerRemote;
import controler.EjbLocator;

@Named("menuBean")
@ApplicationScoped
public class MenuBean {
	private MenuManagerRemote menuManager;
	private Collection<menu.Menu> menus=null;
	
	private Integer idSelected;
	
	private String name;
	private Float price;
	
	public MenuBean()
	{
		listingMenus();
	}
	
	public void listingMenus()
	{	
		//System.out.println("Récupération du moduleManager");
		menuManager=EjbLocator.getLocator().getMenuManager();
		if(menuManager!=null)
		{
			menus=menuManager.all();
		}
	}
	
	public void load ()
    {
    	if (idSelected != null) {
    		Menu menu = menuManager.find(idSelected); 
	        this.setName(menu.getName());
	        this.setPrice(menu.getPrice());
	    }
    }
	
	public void modificate() 
	{
		if (idSelected != null) {
    		Menu menu = menuManager.find(idSelected); 
    		menu.setName(this.name);
    		menu.setPrice(this.price);
    		menuManager.saveOrUpdate(menu);
    		
    		refresh();
    	}
	}
	
	public void refresh()
	{
		if(menuManager!=null)
		{
			name = null;
			price = null;
			
			menus=menuManager.all();
		}
	}
	
	public Collection<menu.Menu> getMenus() {
		return menus;
	}
	public void setMenus(Collection<menu.Menu> menus) {
		this.menus = menus;
	}
	
	public Integer getSelectedMenudId() {
		return idSelected;
	}

	public void setSelectedMenuId(Integer selectedMenuId) {
		idSelected = selectedMenuId;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}
}