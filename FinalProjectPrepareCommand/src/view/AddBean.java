package view;

import java.io.IOException;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import menu.Menu;
import menu.MenuManagerRemote;
import controler.EjbLocator;

@Named("addBean")
@ApplicationScoped
public class AddBean {
	private MenuManagerRemote menuManager;

	private String name;
	private Float price;
	
	public AddBean()
	{
		AddMenu();
	}
	
	public void AddMenu()
	{	
		//System.out.println("Récupération du moduleManager");
		menuManager=EjbLocator.getLocator().getMenuManager();
	}
	
	public void submit()
    {
		Menu menu = new Menu();
    	menu.setName(this.getName());
    	menu.setPrice(this.getPrice());
    	Menu newMenu = this.menuManager.saveOrUpdate(menu);
		
    	name=null;
    	price=null;
    	
		try {
			if(newMenu.getId() > 0) {
				FacesContext.getCurrentInstance().getExternalContext().redirect("addOkMenu.xhtml");
			} else {
				FacesContext.getCurrentInstance().getExternalContext().redirect("addNotOkMenu.xhtml");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}
	

	

}