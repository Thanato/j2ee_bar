package view;

import java.util.Collection;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import menu.Menu;
import menu.MenuManagerRemote;
import controleur.EjbLocator;

@Named("menuBean")
@ApplicationScoped
public class MenuBean {
	private MenuManagerRemote menuManager;
	private Collection<Menu> menus=null;
	
	public MenuBean()
	{
		listingMenus();
	}
	
	public void listingMenus()
	{	
		//System.out.println("Récupération du moduleManager");
		menuManager=EjbLocator.getLocator().getMenuManager();
		if(menuManager!=null)
		{
			menus=menuManager.all();
		}
	}
	
	public void refresh()
	{
		if(menuManager!=null)
		{
			menus=menuManager.all();
		}
	}
	
	public Collection<Menu> getMenus() {
		return menus;
	}
	public void setMenus(Collection<Menu> menus) {
		this.menus = menus;
	}
}

