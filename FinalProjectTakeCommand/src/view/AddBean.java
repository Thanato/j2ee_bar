package view;

import java.io.IOException;
import java.util.Date;
import java.util.ArrayList;
import java.util.Collection;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import command.Command;
import command.CommandManagerRemote;
import menu.Menu;
import menu.MenuManagerRemote;
import controleur.EjbLocator;

@Named("addBean")
@ApplicationScoped
public class AddBean {
	private CommandManagerRemote commandManager;
	private MenuManagerRemote menuManager;
	
	Command command = null; 
	
	private Collection<menu.Menu> menus=null;
	private Collection<Menu> details=null;

	private Menu menu;	
	private Integer tableId;
	
	public AddBean()
	{
		listingMenus();
		prepareCommand();
	}
	
	public void listingMenus()
	{	
		//System.out.println("Récupération du moduleManager");
		commandManager=EjbLocator.getLocator().getCommandManager();
		menuManager=EjbLocator.getLocator().getMenuManager();
		if(menuManager!=null)
		{
			menus=menuManager.all();
		}
	}
	
	public void prepareCommand()
	{
		command = new Command(); 
		
		details = new ArrayList<Menu>();
	}
	
	public void submit()
    {
    	command.setTableId(this.getTableId());
    	command.setTakenAt(new Date());
    	command.setMenus(details);
    	Command newCommand = this.commandManager.saveOrUpdate(command);
		
		try {
			tableId=null;
			details=null;
			if(newCommand.getId() > 0) {
				FacesContext.getCurrentInstance().getExternalContext().redirect("addOkCommand.xhtml");
			} else {
				FacesContext.getCurrentInstance().getExternalContext().redirect("addNotOkCommand.xhtml");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	public void add() 
	{	
		if (details == null) details = new ArrayList<Menu>();
		details.add(menu);
	}
	
	public Collection<Menu> getDetails() {
		return details;
	}

	public void setDetails(Collection<Menu> details) {
		this.details = details;
	}
	
	public Collection<menu.Menu> getMenus() {
		return menus;
	}
	public void setMenus(Collection<menu.Menu> menus) {
		this.menus = menus;
	}

	public Integer getTableId() {
		return tableId;
	}

	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}
}