package view;

import java.util.Collection;
import java.util.Date;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import command.Command;
import command.CommandManagerRemote;
import controleur.EjbLocator;

@Named("indexBean")
@ApplicationScoped
public class IndexBean {
	private CommandManagerRemote commandManager;
	
	private Collection<command.Command> commands=null;
	
	private Integer idSelected;
	
	private Integer id;
	private Integer tableId;
	private Date takenAt;
	private Date readyAt;
	private Collection<menu.Menu> menus;
	
	public IndexBean()
	{
		listingReadyCommands();
	}
	
	public void listingReadyCommands()
	{	
		//System.out.println("Récupération du moduleManager");
		commandManager=EjbLocator.getLocator().getCommandManager();
		if(commandManager!=null)
		{
			commands=commandManager.ready();
		}
	}
	
	public void load ()
    {
    	if (idSelected != null) {
    		Command command = commandManager.find(idSelected); 
	        this.setId(command.getId());
	        this.setTableId(command.getTableId());
	        this.setTakenAt(command.getTakenAt());
	        this.setReadyAt(command.getReadyAt());
	        this.setMenus(command.getMenus());
    	}
    }
	
	public void finish() 
	{
		if (idSelected != null) {
    		Command command = commandManager.find(idSelected); 
    		command.setFinishedAt(new Date());
    		commandManager.saveOrUpdate(command);
    		
    		refresh();
    	}
	}
	
	public void refresh()
	{
		if(commandManager!=null)
		{
			id = null;
			tableId = null;
			takenAt = null;
			readyAt = null;
			menus = null;
			
			commands=commandManager.ready();
		}
	}
	
	public Collection<command.Command> getCommands() {
		return commands;
	}
	public void setCommands(Collection<command.Command> commands) {
		this.commands = commands;
	}
	
	public Integer getSelectedCommandId() {
		return idSelected;
	}

	public void setSelectedCommandId(Integer selectedCommandId) {
		idSelected = selectedCommandId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTableId() {
		return tableId;
	}

	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}



	public Collection<menu.Menu> getMenus() {
		return menus;
	}

	public void setMenus(Collection<menu.Menu> menus) {
		this.menus = menus;
	}

	public Date getTakenAt() {
		return takenAt;
	}

	public void setTakenAt(Date takenAt) {
		this.takenAt = takenAt;
	}

	public Date getReadyAt() {
		return readyAt;
	}

	public void setReadyAt(Date readyAt) {
		this.readyAt = readyAt;
	}
}
