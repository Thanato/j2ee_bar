package converter;

import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Named;

import menu.Menu;
import menu.MenuManagerRemote;

@Named("menuConverter")
@ManagedBean
@RequestScoped
public class MenuConverter implements Converter {

    @EJB
    private MenuManagerRemote MenuManager;


	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		// TODO Auto-generated method stub
		if (arg2 == null || arg2.isEmpty()) {
            return null;
        }

        try {
            return MenuManager.find(Integer.valueOf(arg2));
        } catch (NumberFormatException e) {
            throw new ConverterException(new FacesMessage(String.format("%s is not a valid menu ID", arg2)), e);
        }
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		// TODO Auto-generated method stub
		if (arg2 == null) {
            return "";
        }

        if (arg2 instanceof Menu) {
            return String.valueOf(((Menu) arg2).getId());
        } else {
            throw new ConverterException(new FacesMessage(String.format("%s is not a menu User", arg2)), new NumberFormatException());
        }
	}

}
