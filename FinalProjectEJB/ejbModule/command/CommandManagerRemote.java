package command;

import java.util.Collection;

import javax.ejb.Remote;

@Remote
public interface CommandManagerRemote {
	public Command saveOrUpdate(Command contact);
	public Command find(int id);
	public void delete(Command contact);
	public Collection<Command> all();
	public Collection<Command> ready();
	public Collection<Command> taken();
	public Collection<Command> finish();
}
