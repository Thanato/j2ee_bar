package command;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import menu.Menu;

@Entity
@Table(name="Command")
public class Command implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private Integer tableId;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date takenAt;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date readyAt;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date finishedAt;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date payedAt;
	
	@ManyToMany(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
	@JoinTable(name="detail",
		joinColumns=@JoinColumn(name="command_id", referencedColumnName="id"),
		inverseJoinColumns=@JoinColumn(name="menu_id", referencedColumnName="id")
	)
	public Collection<Menu> menus = new ArrayList<Menu>();
	
	public Collection<Menu> getMenus() {
		return menus;
	}

	public void setMenus(Collection<Menu> menus) {
		this.menus = menus;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public Date getReadyAt() {
		return readyAt;
	}

	public void setReadyAt(Date readyAt) {
		this.readyAt = readyAt;
	}

	public Integer getTableId() {
		return tableId;
	}

	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}

	public Date getFinishedAt() {
		return finishedAt;
	}

	public void setFinishedAt(Date finishedAt) {
		this.finishedAt = finishedAt;
	}
	
	public Date getPayedAt() {
		return payedAt;
	}

	public void setPayedAt(Date payedAt) {
		this.payedAt = payedAt;
	}
	
	public Date getTakenAt() {
		return takenAt;
	}

	public void setTakenAt(Date takenAt) {
		this.takenAt = takenAt;
	}

	@Override
	public String toString() {
		return "Command [id=" + id + ", tableId=" + tableId + "]";
	}
	
}
