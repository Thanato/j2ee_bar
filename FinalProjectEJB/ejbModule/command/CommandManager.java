package command;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

/**
 * Session Bean implementation class CommandManager
 */
@Stateless(mappedName = "CommandManager")
@LocalBean
public class CommandManager implements CommandManagerRemote {

    /**
     * Default constructor. 
     */
    public CommandManager() {
        // TODO Auto-generated constructor stub
    }
    
    @PersistenceContext
	EntityManager em;
	
	@Override
	public Command saveOrUpdate(Command command) {
		if (command.getId() == null) {
			em.persist(command);
			return command;
		} else  {
		    return em.merge(command);
		}
	}

	@Override
	public Collection<Command> all() {
		return em
			.createQuery("SELECT c FROM Command c ORDER BY c.finishedAt, c.tableId DESC", Command.class)
			.getResultList();
	}
	
	@Override
	public Collection<Command> ready() {
		return em
			.createQuery("SELECT c FROM Command c WHERE c.readyAt IS NOT NULL AND c.finishedAt IS NULL", Command.class)
			.getResultList();
	}
	
	@Override
	public Collection<Command> taken() {
		return em
			.createQuery("SELECT c FROM Command c WHERE c.readyAt IS NULL", Command.class)
			.getResultList();
	}
	
	@Override
	public Collection<Command> finish() {
		return em
			.createQuery("SELECT c FROM Command c WHERE c.readyAt IS NOT NULL and c.finishedAt IS NOT NULL and c.payedAt IS NULL", Command.class)
			.getResultList();
	}

	@Override
	public void delete(Command command) {
		em
			.createQuery("DELETE FROM Command i WHERE i.id = :id")
	        .setParameter("id", command.getId())
	        .executeUpdate();
	}

	@Override
	public Command find(int id) {
		return em
			.createQuery("SELECT c FROM Command c WHERE c.id = :id", Command.class)
			.setParameter("id", id)
			.getSingleResult();
	}

}
