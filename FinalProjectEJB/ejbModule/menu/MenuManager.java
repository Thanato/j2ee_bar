package menu;

import java.util.Collection;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Session Bean implementation class MenuManager
 */
@Stateless(mappedName = "MenuManager")
@LocalBean
public class MenuManager implements MenuManagerRemote {

    /**
     * Default constructor. 
     */
    public MenuManager() {
        // TODO Auto-generated constructor stub
    }

    @PersistenceContext
	EntityManager em;
	
	@Override
	public Menu saveOrUpdate(Menu menu) {
		if (menu.getId() == null) {
			em.persist(menu);
			return menu;
		} else  {
		    return em.merge(menu);
		}
	}

	@Override
	public Collection<Menu> all() {
		return em
			.createQuery("SELECT c FROM Menu c", Menu.class)
			.getResultList();
	}

	@Override
	public void delete(Menu menu) {
		em
			.createQuery("DELETE FROM Menu i WHERE i.id = :id")
	        .setParameter("id", menu.getId())
	        .executeUpdate();
	}

	@Override
	public Menu find(int id) {
		return em
			.createQuery("SELECT c FROM Menu c WHERE id = :id", Menu.class)
			.setParameter("id", id)
			.getSingleResult();
	}

}
