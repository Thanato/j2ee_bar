package menu;

import java.util.Collection;
import javax.ejb.Remote;

@Remote
public interface MenuManagerRemote {
	public Menu saveOrUpdate(Menu contact);
	public Menu find(int id);
	public void delete(Menu contact);
	public Collection<Menu> all();
}
