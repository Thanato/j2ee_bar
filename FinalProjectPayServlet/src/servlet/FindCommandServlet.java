package servlet;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import command.Command;
import controler.EjbLocator;

/**
 * Servlet implementation class FindCommandServlet
 */
@WebServlet("/FindCommand")
public class FindCommandServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FindCommandServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			ObjectInputStream ois = new ObjectInputStream(request.getInputStream());
			Integer id = (Integer) ois.readObject();

			Command command = EjbLocator.getLocator().getCommandManager().find(id);
			
			ObjectOutputStream oos = new ObjectOutputStream(response.getOutputStream());
	
			oos.writeObject(command);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
