import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import command.Command;
import command.CommandManager;
import command.CommandManagerRemote;

public class Test {
	
	private static <T> T lookupRemote(Class<T> remote, String implementation) throws NamingException {
		
		final Hashtable<String, String> jndiProperties = new Hashtable<String, String>();
		jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		final Context context = new InitialContext(jndiProperties);
		
		//final String jndi = "ejb:/BarEJB/" + implementation + "!" + remote.getName();
		final String jndi = "ejb:FinalProjectEAR/FinalProjectEJB/CommandManager!command.CommandManagerRemote";
		
		System.out.println("JNDI g�n�r� : " + jndi);

		return (T) context.lookup(jndi);
	}
	
	private static void test() throws NamingException {
		System.out.println("D�part du test");
		
		CommandManagerRemote commandManager = lookupRemote(CommandManagerRemote.class, "CommandManager");
		
		Command command = commandManager.find(0);
		String retour = command.toString();
		//String retour = commandManager.g();
		
		System.out.println("Test d'appel � la table command : " + retour);
	}

	public static void main(String[] args) throws NamingException {
		test();
	}
	

}
