package view;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

public class CommandsTableModel extends AbstractTableModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[] columnNames = { "ID", "Table", "Prise �", "Pr�te �", "Finie �"};
	
	private ArrayList<String> commands;

	public CommandsTableModel() {
		super();
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		return commands.size() / columnNames.length;
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	public Class<? extends Object> getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		String valeur = "";
		int x = columnNames.length * (rowIndex) + columnIndex;
		valeur = commands.get(x);
		return valeur;
	}

	public void setCommands(ArrayList<String> commands) {
		this.commands = commands;
		fireTableDataChanged();
	}
	
	

}
