/*
 * Source bas�e :
 * http://www.java2s.com/Tutorial/Java/0240__Swing/DualListBoxSample.htm
 * */

package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;

public class DualListBox extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Insets EMPTY_INSETS = new Insets(0, 0, 0, 0);

	private static final String ADD_BUTTON_LABEL = "Ajouter >";
	
	private static final String ADD_ALL_BUTTON_LABEL = "Tout ajouter >>";

	private static final String REMOVE_BUTTON_LABEL = "< Retirer";
	
	private static final String REMOVE_ALL_BUTTON_LABEL = "<< Tout retirer";

	private static final String DEFAULT_SOURCE_CHOICE_LABEL = "Consommations (�)";

	private static final String DEFAULT_DEST_CHOICE_LABEL = "Consommations � payer (�)";

	private JLabel sourceLabel;

	private JList<?> sourceList;

	private SortedListModel sourceListModel;
	
	private JList<?> destList;

	private SortedListModel destListModel;

	private JLabel destLabel;

	private JButton addButton;
	private JButton addAllButton;
	private JButton removeButton;
	private JButton removeAllButton;

	public DualListBox() {
		initScreen();
	}
	
	public JList<?> getSourceList()
	{
		return sourceList;
	}
	
	public JList<?> getDestList()
	{
		return destList;
	}
	
	public SortedListModel getSourceListModel()
	{
		return sourceListModel;
	}
	
	public SortedListModel getDestListModel()
	{
		return destListModel;
	}

	public String getSourceChoicesTitle() {
		return sourceLabel.getText();
	}

	public void setSourceChoicesTitle(String newValue) {
	  sourceLabel.setText(newValue);
  	}

	public String getDestinationChoicesTitle() {
	  return destLabel.getText();
  	}

	public void setDestinationChoicesTitle(String newValue) {
	  destLabel.setText(newValue);
  	}

	public void clearSourceListModel() {
	  sourceListModel.clear();
  	}

	public void clearDestinationListModel() {
	  destListModel.clear();
  	}

	public void addSourceElements(ListModel<?> newValue) {
	  fillListModel(sourceListModel, newValue);
  	}

	public void setSourceElements(ListModel<?> newValue) {
	  clearSourceListModel();
    	addSourceElements(newValue);
  	}

	public void addDestinationElements(ListModel<?> newValue) {
		fillListModel(destListModel, newValue);
	}

	private void fillListModel(SortedListModel model, ListModel<?> newValues) {
		int size = newValues.getSize();
		for (int i = 0; i < size; i++) {
			model.add(newValues.getElementAt(i));
		}
	}

	public void addSourceElements(Object newValue[]) {
		fillListModel(sourceListModel, newValue);
	}

	public void setSourceElements(Object newValue[]) {
		clearSourceListModel();
		addSourceElements(newValue);
	}

	public void addDestinationElements(Object newValue[]) {
		fillListModel(destListModel, newValue);
	}

	private void fillListModel(SortedListModel model, Object newValues[]) {
		model.addAll(newValues);
	}

	public Iterator sourceIterator() {
		return sourceListModel.iterator();
	}

	public Iterator destinationIterator() {
		return destListModel.iterator();
	}

	public void setSourceCellRenderer(ListCellRenderer newValue) {
		sourceList.setCellRenderer(newValue);
	}

	public ListCellRenderer getSourceCellRenderer() {
		return sourceList.getCellRenderer();
	}

	public void setDestinationCellRenderer(ListCellRenderer newValue) {
		destList.setCellRenderer(newValue);
	}

	public ListCellRenderer getDestinationCellRenderer() {
		return destList.getCellRenderer();
	}

	public void setVisibleRowCount(int newValue) {
		sourceList.setVisibleRowCount(newValue);
		destList.setVisibleRowCount(newValue);
	}

	public int getVisibleRowCount() {
		return sourceList.getVisibleRowCount();
	}

	public void setSelectionBackground(Color newValue) {
		sourceList.setSelectionBackground(newValue);
		destList.setSelectionBackground(newValue);
	}

	public Color getSelectionBackground() {
		return sourceList.getSelectionBackground();
	}

	public void setSelectionForeground(Color newValue) {
		sourceList.setSelectionForeground(newValue);
		destList.setSelectionForeground(newValue);
	}

	public Color getSelectionForeground() {
		return sourceList.getSelectionForeground();
	}

	public void clearSourceSelected() {
		Object selected[] = sourceList.getSelectedValues();
		for (int i = selected.length - 1; i >= 0; --i) {
			sourceListModel.removeElement(selected[i]);
		}
		sourceList.getSelectionModel().clearSelection();
	}

	public void clearDestinationSelected() {
		Object selected[] = destList.getSelectedValues();
		for (int i = selected.length - 1; i >= 0; --i) {
			destListModel.removeElement(selected[i]);
		}
		destList.getSelectionModel().clearSelection();
	}

	private void initScreen() {
		setBorder(BorderFactory.createEtchedBorder());
		setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.fill = GridBagConstraints.BOTH;
		addButton = new JButton(ADD_BUTTON_LABEL);
		gbc.gridx = 1;
		gbc.gridy = 1;
		add(addButton, gbc);
    
		addAllButton = new JButton(ADD_ALL_BUTTON_LABEL);
		gbc.gridx = 1;
		gbc.gridy = 2;
		add(addAllButton, gbc);
    
		removeButton = new JButton(REMOVE_BUTTON_LABEL);   
		gbc.gridx = 1;
		gbc.gridy = 3;
		add(removeButton, gbc);
    
		removeAllButton = new JButton(REMOVE_ALL_BUTTON_LABEL);
		gbc.gridx = 1;
		gbc.gridy = 4;
		add(removeAllButton, gbc);
		
		sourceLabel = new JLabel(DEFAULT_SOURCE_CHOICE_LABEL);
		sourceListModel = new SortedListModel();
		sourceList = new JList(sourceListModel);
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.NONE;
		gbc.insets = EMPTY_INSETS;
		add(sourceLabel, gbc);
		
		destLabel = new JLabel(DEFAULT_DEST_CHOICE_LABEL);
		destListModel = new SortedListModel();
		destList = new JList(destListModel);
		gbc.gridx = 2;
		gbc.gridy = 0;
		add(destLabel, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.gridheight = 5;
		gbc.weightx = .5;
		gbc.weighty = 1;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = EMPTY_INSETS;
		add(new JScrollPane(sourceList), gbc);
    
		gbc.gridx = 2;
		gbc.gridy = 1;
		add(new JScrollPane(destList), gbc);
	}
	
	public void setAddActionListener(ActionListener al)
	{
		addButton.addActionListener(al);
	}
	
	public void setAddAllActionListener(ActionListener al)
	{
		addAllButton.addActionListener(al);
	}
	
	public void setRemoveActionListener(ActionListener al)
	{
		removeButton.addActionListener(al);
	}
	
	public void setRemoveAllActionListener(ActionListener al)
	{
		removeAllButton.addActionListener(al);
	}
}
