package view;

import static javax.swing.JOptionPane.showMessageDialog;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import command.Command;

public class EditCustomer extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public EditCustomer(Command command) {
		super();
		InitCustomer(command);
	}
	
	public void InitCustomer(Command command) {
		getContentPane().setBackground(Color.WHITE);
		setBounds(100, 100, 320, 260);
		getContentPane().setLayout(null);
		
		JLabel title = new JLabel("Edtion de facture");
		title.setFont(new Font("Tahoma", Font.BOLD, 18));
		title.setBounds(10, 10, 277, 20);
		getContentPane().add(title);
		
		JLabel name = new JLabel("Nom");
		name.setFont(new Font("Tahoma", Font.BOLD, 15));
		name.setBounds(10, 40, 290, 20);
		getContentPane().add(name);
		
		JTextField text_name = new JTextField();
		text_name.setBounds(10, 65, 290, 20);
		getContentPane().add(text_name);
		
		JLabel address = new JLabel("Adresse");
		address.setFont(new Font("Tahoma", Font.BOLD, 15));
		address.setBounds(10, 90, 290, 20);
		getContentPane().add(address);
		
		JTextField text_address = new JTextField();
		text_address.setBounds(10, 115, 290, 20);
		getContentPane().add(text_address);
		
		JLabel ville = new JLabel("Ville");
		ville.setFont(new Font("Tahoma", Font.BOLD, 15));
		ville.setBounds(10, 140, 175, 20);
		getContentPane().add(ville);
		
		JTextField text_ville = new JTextField();
		text_ville.setBounds(10, 165, 175, 20);
		getContentPane().add(text_ville);
		
		JLabel cp = new JLabel("Code postal");
		cp.setFont(new Font("Tahoma", Font.BOLD, 15));
		cp.setBounds(185, 140, 115, 20);
		getContentPane().add(cp);
		
		JTextField text_cp = new JTextField();
		text_cp.setBounds(185, 165, 115, 20);
		getContentPane().add(text_cp);
		
		JButton edit_button = new JButton("Editer la facture");
		edit_button.setBounds(10, 190, 290, 20);
		edit_button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (text_name.getText() != "" && text_address.getText() != "" && text_ville.getText() != "" && text_cp.getText() != "") {
					EditFacture edit = new EditFacture(command, text_name.getText(), text_address.getText(), text_ville.getText(), text_cp.getText());
					
					edit.setVisible(true);
					
					dispose();
				}
				else
				{
					showMessageDialog(null, "Saisissez tous les champs");
				}
			}
			
		});
		getContentPane().add(edit_button);
	}
	
}
