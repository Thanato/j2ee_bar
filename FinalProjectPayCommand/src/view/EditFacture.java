package view;

import static javax.swing.JOptionPane.showMessageDialog;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import command.Command;
import menu.Menu;

public class EditFacture extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public EditFacture(Command command, String name, String address, String cp, String city)
	{
		super();
		InitFacture(command, name, address, cp, city);
	}
	
	public void InitFacture(Command command, String name, String address, String cp, String city)
	{
		getContentPane().setBackground(Color.WHITE);
		setBounds(100, 100, 330, 400);
		getContentPane().setLayout(null);
		
		JLabel title = new JLabel("Facture n�" + command.getId());
		title.setFont(new Font("Tahoma", Font.BOLD, 18));
		title.setBounds(10, 10, 277, 14);
		getContentPane().add(title);
		
		JLabel date = new JLabel(LocalDate.now().toString());
		date.setFont(new Font("Tahoma", Font.BOLD, 18));
		date.setBounds(200, 10, 277, 14);
		getContentPane().add(date);
		
		JLabel barName = new JLabel("Bar d'Amiens");
		barName.setFont(new Font("Tahoma", 0, 14));
		barName.setBounds(10, 50, 277, 14);
		getContentPane().add(barName);
		
		JLabel barAddress = new JLabel("15 rue du Java");
		barAddress.setFont(new Font("Tahoma", 0, 14));
		barAddress.setBounds(10, 65, 277, 14);
		getContentPane().add(barAddress);
		
		JLabel barVille = new JLabel("80000 Amiens");
		barVille.setFont(new Font("Tahoma", 0,  14));
		barVille.setBounds(10, 80, 277, 14);
		getContentPane().add(barVille);
		
		JLabel factureA = new JLabel("Factur� �");
		factureA.setFont(new Font("Tahoma", Font.BOLD, 16));
		factureA.setBounds(10, 120, 277, 14);
		getContentPane().add(factureA);
		
		JLabel factureAName = new JLabel(name);
		factureAName.setFont(new Font("Tahoma", 0, 14));
		factureAName.setBounds(10, 135, 277, 14);
		getContentPane().add(factureAName);
		
		JLabel factureAAddress = new JLabel(address);
		factureAAddress.setFont(new Font("Tahoma", 0, 14));
		factureAAddress.setBounds(10, 150, 277, 14);
		getContentPane().add(factureAAddress);
		
		JLabel factureAVille = new JLabel(cp + " " + city);
		factureAVille.setFont(new Font("Tahoma", 0,  14));
		factureAVille.setBounds(10, 165, 277, 14);
		getContentPane().add(factureAVille);
				
		JLabel detail = new JLabel("D�tail de la commande");
		detail.setFont(new Font("Tahoma", Font.BOLD,  16));
		detail.setBounds(10, 200, 277, 14);
		getContentPane().add(detail);
		
		int y = 230;
		float montant = 0;
		for (Menu item : command.getMenus())
		{
			JLabel dn = new JLabel(item.getName());
			dn.setFont(new Font("Tahoma", 0,  10));
			dn.setBounds(10, y, 277, 14);
			
			JLabel dp = new JLabel(item.getPrice() + " �");
			dp.setFont(new Font("Tahoma", 0,  10));
			dp.setBounds(150, y, 277, 14);
			
			getContentPane().add(dn);
			getContentPane().add(dp);
			
			montant = montant + item.getPrice();
			
			y = y + 20;
		}
		
		JLabel ttc = new JLabel("Montant TTC : " + montant + " �");
		ttc.setFont(new Font("Tahoma", Font.BOLD, 14));
		ttc.setBounds(10, y, 277, 14);
		getContentPane().add(ttc);

		y = y + 20;
		
		JButton edit_button = new JButton("Imprimer");
		edit_button.setBounds(10, y, 290, 20);
		edit_button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				dispose();
			}
			
		});
		getContentPane().add(edit_button);
	}

}
