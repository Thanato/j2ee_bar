package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import static javax.swing.JOptionPane.showMessageDialog;

import command.Command;
import menu.Menu;

public class ClientMain {
	
	private ArrayList<String> commands = new ArrayList<>();
	private Collection<command.Command> readCommands;
	
	private JFrame frame;
	private JTable table;
	private JLabel resteAPayer;
	private JLabel aPayer;
	
	private JButton payedButton;
	private JButton editButton;
	
	private Command currentCommand;
	
	private CommandsTableModel commandsTable = new CommandsTableModel();
	private DualListBox dual = new DualListBox(); 
	
	public static void main(String[] args) throws Exception {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientMain window = new ClientMain();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public ClientMain() {
		// INITIALISATION DE LA FENETRE
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 623, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		// TABLEAU ARTICLES
		JLabel labelListeDesCommandes = new JLabel("LISTE DES COMMANDES");
		labelListeDesCommandes.setFont(new Font("Tahoma", Font.BOLD, 14));
		labelListeDesCommandes.setBounds(10, 10, 277, 14);
		frame.getContentPane().add(labelListeDesCommandes);
		
		JButton refresh_btn = new JButton("Rafraichir");
		refresh_btn.setBounds(495, 10, 100, 20);
		refresh_btn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				refresh();
			}
			
		});
		frame.getContentPane().add(refresh_btn);
		

		commandsTable.setCommands(commands);
		table = new JTable(commandsTable);
		table.setRowSelectionAllowed(false);
		table.setBackground(new Color(204, 204, 153));
		
		table.addMouseListener(new MouseAdapter() {
		    @Override
		    public void mouseClicked(MouseEvent evt) {
		        int row = table.rowAtPoint(evt.getPoint());
		        int col = table.columnAtPoint(evt.getPoint());
		        if (row >= 0 && col >= 0) {
		        	try {
		        		currentCommand = getCommand(Integer.parseInt((String)table.getValueAt(row, 0)));
		        		Collection<Menu> menus = currentCommand.getMenus();
		        		String[] details = new String[menus.size()];
		        		int i = 0;
		        		for (Menu item : menus) {
		        			details[i] = item.getName() + ", " + item.getPrice().toString();
		        			i++;
		        		}
						dual.setSourceElements(details);
						resteAPayer.setText("Reste � payer " + calculMontantReste() + " �");
						aPayer.setText("A payer " + calculMontantAPayer() + " �");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }
		    }
		});
		
		TableColumn column = null;
		column = table.getColumnModel().getColumn(0);
		column.setPreferredWidth(33);
		column = table.getColumnModel().getColumn(1);
		column.setPreferredWidth(41);
		column = table.getColumnModel().getColumn(2);
		column.setPreferredWidth(170);
		column = table.getColumnModel().getColumn(3);
		column.setPreferredWidth(170);
		column = table.getColumnModel().getColumn(4);
		column.setPreferredWidth(170);
		JScrollPane scrollPaneTable = new JScrollPane(table);
		table.setFillsViewportHeight(true);
		table.setAutoResizeMode(0);
		scrollPaneTable.setBounds(10, 35, 587, 206);
		frame.getContentPane().add(scrollPaneTable);
		
		// DUAL LIST BOX
	    dual.setBounds(10, 250, 587, 150);
	    dual.setAddActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				@SuppressWarnings("deprecation")
				Object selected[] = dual.getSourceList().getSelectedValues();
				dual.addDestinationElements(selected);
				dual.clearSourceSelected();
				resteAPayer.setText("Reste � payer " + calculMontantReste() + " �");
				aPayer.setText("A payer " + calculMontantAPayer() + " �");
			}   	
	    });
	    dual.setAddAllActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				dual.addDestinationElements(dual.getSourceListModel());
				dual.clearSourceListModel();
				resteAPayer.setText("Reste � payer " + calculMontantReste() + " �");
				aPayer.setText("A payer " + calculMontantAPayer() + " �");
			}	    	
	    });
	    dual.setRemoveActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				@SuppressWarnings("deprecation")
				Object selected[] = dual.getDestList().getSelectedValues();
				dual.addSourceElements(selected);
				dual.clearDestinationSelected();
				resteAPayer.setText("Reste � payer " + calculMontantReste() + " �");
				aPayer.setText("A payer " + calculMontantAPayer() + " �");
			}   	
	    });
	    dual.setRemoveAllActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				dual.addSourceElements(dual.getDestListModel());
				dual.clearDestinationListModel();
				resteAPayer.setText("Reste � payer " + calculMontantReste() + " �");
				aPayer.setText("A payer " + calculMontantAPayer() + " �");
			}	    	
	    });
	    frame.getContentPane().add(dual, BorderLayout.CENTER);
	    
	    // PAYEMENT
	    resteAPayer = new JLabel("Reste � payer 0 �");
	    resteAPayer.setFont(new Font("Tahoma", Font.BOLD, 14));
	    resteAPayer.setBounds(10, 410, 277, 16);
	    frame.getContentPane().add(resteAPayer);
	    
	    aPayer = new JLabel("A payer 0 �");
	    aPayer.setFont(new Font("Tahoma", Font.BOLD, 14));
	    aPayer.setBounds(350, 410, 277, 16);
		frame.getContentPane().add(aPayer);
		
		// BOUTON
		payedButton = new JButton("Payer");
		payedButton.setBounds(290, 435, 150, 20);
		payedButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (dual.getSourceList().getModel().getSize() > 0)
				{
					dual.clearDestinationListModel();
					aPayer.setText("A payer " + calculMontantAPayer() + " �");
				}
				else
				{
					try {
						if (setCommand(currentCommand.getId()))
						{
							showMessageDialog(null, "Commande encaiss�e");
							dual.clearDestinationListModel();
							refresh();
						}
						else
						{
							showMessageDialog(null, "Une erreur est survenue dans l'encaissement de la commande");
						}
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
			}			
		});
		frame.getContentPane().add(payedButton);
		
		editButton = new JButton("Editer");
		editButton.setBounds(445, 435, 150, 20);
		editButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (currentCommand != null) {
					EditCustomer edit = new EditCustomer(currentCommand);
					
					edit.setVisible(true);
				}
			}
			
		});
		frame.getContentPane().add(editButton);
		
		// INITIALISATION
		refresh();
	}
	
	private float calculMontantReste()
	{
		float montant = 0;
	    for (int i = 0; i < dual.getSourceList().getModel().getSize(); i++) 
	    {
	    	montant = montant + Float.parseFloat(((String)dual.getSourceList().getModel().getElementAt(i)).split(",")[1]);
	    }
	    return montant;
	}
	
	private float calculMontantAPayer()
	{
		float montant = 0;
	    for (int i = 0; i < dual.getDestList().getModel().getSize(); i++) 
	    {
	    	montant = montant + Float.parseFloat(((String)dual.getDestList().getModel().getElementAt(i)).split(",")[1]);
	    }
	    return montant;
	}
	
	private void refresh() {		
		ArrayList<String> commands = new ArrayList<>();
		try {
			readCommands = getCommands();
			
			for (Command item : readCommands) {
				commands.add(item.getId().toString());
				commands.add(item.getTableId().toString());
				commands.add(item.getTakenAt().toString());
				commands.add(item.getReadyAt().toString());
				commands.add(item.getFinishedAt().toString());
			}

			commandsTable.setCommands(commands);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Collection<command.Command> getReadCommands() {
		return readCommands;
	}

	public void setReadCommands(Collection<command.Command> readCommands) {
		this.readCommands = readCommands;
	}
	
	private static URLConnection getConnection(URL url) throws Exception {
    	// Connexion � la servlet
        //URL url = new URL("http://localhost:8080/FinalProjectPayServlet/ListCommands");
        URLConnection connexion = url.openConnection();
        connexion.setDoOutput(true);
        
        return connexion;
    }
    
	public static Collection<Command> getCommands() throws Exception
    {
        ObjectInputStream fluxentree = new ObjectInputStream(getConnection(new URL("http://localhost:8080/FinalProjectPayServlet/ListCommands")).getInputStream());
        return (Collection<Command>) fluxentree.readObject();
    }
    
    public static Command getCommand(int id) throws Exception
    {
    	URLConnection connection = getConnection(new URL("http://localhost:8080/FinalProjectPayServlet/FindCommand"));
    	
    	ObjectOutputStream fluxsortie = new ObjectOutputStream(connection.getOutputStream());    	
    	fluxsortie.writeObject(id);
    	
    	ObjectInputStream fluxentree = new ObjectInputStream(connection.getInputStream());
    	return (Command) fluxentree.readObject();
    }
    
    public static boolean setCommand(int id) throws Exception {
    	URLConnection connection = getConnection(new URL("http://localhost:8080/FinalProjectPayServlet/PaidCommand"));
    	
    	ObjectOutputStream fluxsortie = new ObjectOutputStream(connection.getOutputStream());    	
    	fluxsortie.writeObject(id);
    	
    	ObjectInputStream fluxentree = new ObjectInputStream(connection.getInputStream());
    	return (boolean) fluxentree.readObject();
    }

}
