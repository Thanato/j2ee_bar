<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*" %>
<%@ page import="command.Command" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Accueil</title>
</head>
<body>
	<h1>Gestionnaire des commandes</h1>
     <ul>
     	<li/><a href="ListMenu"/>Menu</a><br/>
        <li/><a href="AddCommand"/>Ajouter une commande</a><br/>
        <li/><a href="ListCommandCours"/>Liste des commandes en cours</a><br/>
     </ul>
     
     <h3>Commandes pr�tes</h3>
     <table width="90%" >
		<%
		Iterator it = ((Collection)request.getAttribute("listCommands")).iterator();
		while(it.hasNext()) {
		Command command = (Command)it.next();
		%>
			<tr>
				<td>Commande N�<%= command.getId() %></td>
				<td>Table N�<%= command.getTableId() %></td>
				<td>Pr�te depuis <%= command.getReadyAt() %></td>
			</tr>
		<%
		}
		%>
	</table>
</body>
</html>