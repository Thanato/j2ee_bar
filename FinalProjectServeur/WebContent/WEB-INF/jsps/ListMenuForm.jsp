<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*" %>
<%@ page import="menu.Menu" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Menu</title>
</head>
<body>
	<h1>Liste du menu</h1>
	<table width="90%" >
		<%
		Iterator it = ((Collection)request.getAttribute("listMenus")).iterator();
		while(it.hasNext()) {
		Menu menu = (Menu)it.next();
		%>
			<tr>
				<td>Nom <%= menu.getName() %></td>
				<td>Prix <%= menu.getPrice() %> €</td>
			</tr>
		<%
		}
		%>
	</table>
	
	<ul>
        <li/><a href="AddCommand"/>Ajouter une commande</a><br/>
        <li/><a href="Accueil"/>Retour</a><br/>
     </ul>
</body>
</html>