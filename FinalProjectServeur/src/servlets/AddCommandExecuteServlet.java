package servlets;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import command.Command;
import command.CommandManagerRemote;
import controleur.EjbLocator;

/**
 * Servlet implementation class AddCommandExecuteServlet
 */
@WebServlet("/AddCommandExecute")
public class AddCommandExecuteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddCommandExecuteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsps/AddCommandForm.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Command command = new Command();
		command.setTableId(Integer.parseInt(request.getParameter("command.table")));
		command.setTakenAt(Date.valueOf(LocalDate.now()));
		
		CommandManagerRemote commandManagerRemote = EjbLocator.getLocator().getCommandManager();
		Command newCommand = commandManagerRemote.saveOrUpdate(command);
		RequestDispatcher rd = null;
		if(newCommand.getId() > 0) {
			rd = request.getRequestDispatcher("/WEB-INF/jsps/CommandAddedForm.jsp");
		}
		else {
			rd = request.getRequestDispatcher("/WEB-INF/jsps/CommandNotAddedForm.jsp");
		}
		rd.forward(request, response);
	}

}
